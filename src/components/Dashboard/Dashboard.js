import React, { Component } from 'react';
import Header from "../../elements/Header";
import Sidebar from "../../elements/Sidebar";
import { Link } from 'react-router-dom';

export default class Dashboard extends Component {
	render() {
		return (
			<div>
				<Header />
				<div id="wrapper">
					<Sidebar></Sidebar>
				</div>
			</div>
		)
	}
}