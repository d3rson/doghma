import React, { Component } from "react";
import axios from "axios";
import { Redirect } from "react-router-dom";
import TitleComponent from "../title";

import waveImg from "../../assets/img/wave.png";
import avatarImg from "../../assets/img/avatar.svg";
import settingsImg from "../../assets/img/settings.svg";

import './UserLogin.scss';

export default class Login extends Component {

	state = {
		email: "",
		password: "",
		redirect: false,
		authError: false,
		isLoading: false,
		inputFocus: ""
	};

	handleEmailChange = event => {
		this.setState({ email: event.target.value });
	};
	handlePwdChange = event => {
		this.setState({ password: event.target.value });
	};

	handleFieldFocus = (name, status) => {
		this.setState({
			[name + 'Focused']: status
		})
	}

	handleSubmit = event => {
		event.preventDefault();
		this.setState({ isLoading: true });
		const url = "https://doghma-api.herokuapp.com/authenticate";
		const email = this.state.email;
		const password = this.state.password;

		let postData = {
			email: email,
			password: password
		};

		let axiosConfig = {
			headers: {
				"Content-Type": "application/json;charset=UTF-8",
				"Access-Control-Allow-Origin": "*"
			}
		};

		axios
			.post(url, postData, axiosConfig)
			.then(result => {
				console.log(result);
				console.log(result.data.token);
				console.log(result.data.status);
				if (result.status) {
					localStorage.setItem("token", result.data.token);
					this.setState({ redirect: true, isLoading: false });
					localStorage.setItem("isLoggedIn", true);
				}
			})
			.catch(error => {
				console.log(error);
				this.setState({ authError: true, isLoading: false });
			});
	};

	renderRedirect = () => {
		if (this.state.redirect) {
			console.log(this.state.redirect);
			return <Redirect to="/dashboard" />;
		}
	};

	render() {
		const isLoading = this.state.isLoading;

		return (
			<div className="login">
				<TitleComponent title="DOGHMA - Sistema Administrativo"></TitleComponent>
				<img className="wave" src={waveImg} alt="Wave" />
				<div className="container">
					<div className="img">
						<img src={settingsImg} alt="Settings" />
					</div>
					<div className="login-content">
						<form onSubmit={this.handleSubmit}>
							<img src={avatarImg} alt="Avatar" />
							<h2>DOGHMA</h2>
							<div className={"input-div " + (this.state.emailFocused ? "focus" : "")}>
								<div className="i">
									<i className="fas fa-user"></i>
								</div>
								<div>
									<h5>E-mail</h5>
									<input className={"input " + (this.state.authError ? "is-invalid" : "")}
										id="inputEmail"
										name="email"
										onChange={this.handleEmailChange}
										onFocus={() => this.handleFieldFocus("email", true)}
										onBlur={() => this.handleFieldFocus('email', false)}
										required
										type="text" />
									<div className="invalid-feedback">
										Digite um e-mail válido!
             						</div>
								</div>
							</div>
							<div className={"input-div " + (this.state.passwordFocused ? "focus" : "")}>
								<div className="i">
									<i className="fas fa-lock"></i>
								</div>
								<div>
									<h5>Senha</h5>
									<input
										type="password"
										className={
											"input " +
											(this.state.authError ? "is-invalid" : "")
										}
										id="inputPassword"
										name="password"
										onChange={this.handlePwdChange}
										onFocus={() => this.handleFieldFocus("password", true)}	
										onBlur={() => this.handleFieldFocus("password", true)}
										required
									/>
									<div className="invalid-feedback">Senha incorreta!</div>
								</div>
							</div>

							<button className="btn-login" type="submit" disabled={this.state.isLoading ? true : false}>
								Login {isLoading ? (<span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
								) : (<span></span>)}
							</button>
						</form>
					</div>
				</div>
				{this.renderRedirect()}
			</div>
		);
	}
}
