import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import UserLogin from "./components/User/UserLogin";
import UserList from "./components/User/UserList";
import Dashboard from "./components/Dashboard/Dashboard";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <Switch>
            <Route exact path="/" component={UserLogin} />
            <Route path="/usuarios" component={UserList} />
            <Route path='/dashboard' component={Dashboard} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
