import React, {Component} from 'react';
import {Link} from 'react-router-dom';

export default class Sidebar extends Component {
    render() {
        return (
            <div id="wrapper">
                <ul className="sidebar navbar-nav">
                    <li className="nav-item active">
                        <Link to={'/dashboard'} className="nav-link"><i className="fas fa-fw fa-tachometer-alt"></i>
                            <span>&nbsp;Dashboard</span></Link>
                    </li>
                    <li className="nav-item">
                        <Link to={'/clientes'} className="nav-link"><i className="fas fa-fw fa-users"></i>
                            <span>&nbsp;Clientes</span></Link>
                    </li>
                    <li className="nav-item">
                        <Link to={'/dominios'} className="nav-link"><i className="fas fa-fw fa-globe"></i>
                            <span>&nbsp;Domínios</span></Link>
                    </li>
                </ul>
            </div>
        );
    }
}